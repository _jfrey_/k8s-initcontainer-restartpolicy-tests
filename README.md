# Test Pod life with initContainers and restartPolicy strategies

## What the doc say

Kubernetes Doc : https://kubernetes.io/docs/concepts/workloads/pods/_print/#understanding-init-containers

> Note 1 : If a Pod's init container fails, the kubelet repeatedly restarts that init container until it succeeds. However, if the Pod has a restartPolicy of Never, and an init container fails during startup of that Pod, Kubernetes treats the overall Pod as failed.

and https://kubernetes.io/docs/concepts/workloads/pods/init-containers/#detailed-behavior  

> Note 2 : Each init container must exit successfully before the next container starts. If a container fails to start due to the runtime or exits with failure, it is retried according to the Pod restartPolicy. However, if the Pod restartPolicy is set to Always, the init containers use restartPolicy OnFailure.

## Let's try

### 1 pod + 2 initContainers + restartPolicy == Never

#### All InitContainers OK

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-never
  labels:
    app.kubernetes.io/name: test-pod-n
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo Catch them all !!! && sleep 360']
  initContainers:
  - name: init-1
    image: busybox:1.28
    command: ['sh', '-c', "sleep 10"]
  - name: init-2
    image: busybox:1.28
    command: ['sh', '-c', "sleep 5 && exit 0"]
  restartPolicy: Never
```


`kubectl apply -f pod-initContainer-never.yml`  
`kubectl get -f pod-initContainer-never.yml`  
`kubectl describe -f pod-initContainer-never.yml`  


```
Events: 
 Normal  Created    2m5s  kubelet            Created container init-1
 Normal  Started    2m5s  kubelet            Started container init-1
 Normal  Pulled     119s  kubelet            Container image "busybox:1.28" already present on machine
 Normal  Created    119s  kubelet            Created container init-2
 Normal  Started    119s  kubelet            Started container init-2
 Normal  Pulled     118s  kubelet            Container image "busybox:1.28" already present on machine
 Normal  Created    118s  kubelet            Created container myapp-container
 Normal  Started    118s  kubelet            Started container myapp-container
```

=> Pod running

#### 1 InitContainer failed

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-never
  labels:
    app.kubernetes.io/name: test-pod-n
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo Catch them all !!! && sleep 360']
  initContainers:
  - name: init-1
    image: busybox:1.28
    command: ['sh', '-c', "sleep 10"]
  - name: init-2
    image: busybox:1.28
    command: ['sh', '-c', "sleep 5 && exit 255"]
  restartPolicy: Never
```

`kubectl apply -f pod-initContainer-never-failed.yml`  

`kubectl get -f pod-initContainer-never-failed.yml`  

```
test-pod-never   0/1     Init:0/2   0          11s
test-pod-never   0/1     Init:1/2   0          13s
test-pod-never   0/1     Init:Error   0          85s
```

`kubectl describe -f pod-initContainer-never-failed.yml`  

```
init-1:
  State:          Terminated
    Reason:       Completed
    Exit Code:    0
  Restart Count:  0
init-2:
  State:          Terminated
    Reason:       Error
    Exit Code:    255
  Restart Count:  0
myapp-container:
  State:          Terminated
    Reason:       ContainerStatusUnknown
  Restart Count:  0

------
Events:

Normal   Pulled       3m49s                  kubelet            Container image "busybox:1.28" already present on machine
  Normal   Created      3m49s                  kubelet            Created container init-1
  Normal   Started      3m48s                  kubelet            Started container init-1
  Normal   Pulled       3m37s                  kubelet            Container image "busybox:1.28" already present on machine
  Normal   Created      3m37s                  kubelet            Created container init-2
  Normal   Started      3m37s                  kubelet            Started container init-2
  Warning  FailedMount  3m31s (x2 over 3m31s)  kubelet            MountVolume.SetUp failed for volume
```



### 1 pod + 2 initContainers + restartPolicy == Always

> See Note 2 : initContainers will have a restartPolicy to _OnFailure_

#### All InitContainers OK

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-never
  labels:
    app.kubernetes.io/name: test-pod-n
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo Catch them all !!! && sleep 360']
  initContainers:
  - name: init-1
    image: busybox:1.28
    command: ['sh', '-c', "sleep 10"]
  - name: init-2
    image: busybox:1.28
    command: ['sh', '-c', "sleep 5 && exit 0"]
  restartPolicy: Always
```

`kubectl apply -f pod-initContainer-always.yml`  
`kubectl get -f pod-initContainer-always.yml`  
`kubectl describe -f pod-initContainer-always.yml`  

=> Pod running

#### 1 InitContainer failed

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pod-never
  labels:
    app.kubernetes.io/name: test-pod-n
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo Catch them all !!! && sleep 360']
  initContainers:
  - name: init-1
    image: busybox:1.28
    command: ['sh', '-c', "sleep 10"]
  - name: init-2
    image: busybox:1.28
    command: ['sh', '-c', "sleep 5 && exit 255"]
  restartPolicy: Always
```

`kubectl apply -f pod-initContainer-always-failed.yml`  

`kubectl get -f pod-initContainer-always-failed.yml`  

```
NAME             READY   STATUS       RESTARTS      AGE
test-pod-never   0/1     Init:Error   3 (39s ago)   81s
```

`kubectl describe -f pod-initContainer-always-failed.yml`  

```
init-1:
  State:          Terminated
    Reason:       Completed
    Exit Code:    0
  Restart Count:  0
init-2:
  State:          Waiting
    Reason:       CrashLoopBackOff
    Exit Code:    255
  Restart Count:  3
myapp-container:
  State:          Waiting
    Reason:       PodInitializing
  Restart Count:  0

------
Events:

  Normal   Pulled     3m29s                kubelet            Container image "busybox:1.28" already present on machine
  Normal   Created    3m29s                kubelet            Created container init-1
  Normal   Started    3m29s                kubelet            Started container init-1
  Normal   Pulled     94s (x5 over 3m19s)  kubelet            Container image "busybox:1.28" already present on machine
  Normal   Created    94s (x5 over 3m18s)  kubelet            Created container init-2
  Normal   Started    94s (x5 over 3m18s)  kubelet            Started container init-2
  Warning  BackOff    88s (x7 over 3m7s)   kubelet            Back-off restarting failed container
```

## Conclusion 

__restartPolicy == Never__

-> initContainers failed -> pod 'failed'


__restartPolicy != Never__

-> initContainers failed -> restart failed initContainers


:-)
